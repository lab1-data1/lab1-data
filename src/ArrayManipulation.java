import java.util.Arrays;

public class ArrayManipulation {
        static int sum;
        static double max = 0.0;

    public static void main(String[] args) throws Exception {
        int [] numbers = {5,8,3,2,7};
        for(int i= 0;i<numbers.length;i++){
            System.out.println(numbers[i]);
        }

        System.out.println("----------------------------");

        String [] names = {"Alice", "Bob", "Charlie", "David"};
        for(int i = 0;i<names.length;i++){
            System.out.println(names[i]);
        }

        System.out.println("----------------------------");

        double [] Values = new double[4];

        Values[0] = 1.1;
        Values[1] = 2.2;
        Values[2] = 3.3;
        Values[3] = 4.4;

        for(int j = 0;j<numbers.length;j++){
            sum += numbers[j];
        }
        System.out.println(sum);

        System.out.println("----------------------------");

        for(int j = 0;j<Values.length;j++){
            if(Values[j]>max){
                max = Values[j];
            }
        }
        System.out.println(max);

        System.out.println("----------------------------");

        String [] reversedNames = {"Alice", "Bob", "Charlie", "David"};
        for(int i = 0;i<reversedNames.length;i++){
            System.out.println(reversedNames[reversedNames.length - i - 1]);
        }

        System.out.println("----------------------------");

        for(int i=0;i<numbers.length;i++){
            Arrays.sort(numbers);
            System.out.println(numbers[i]);
        }
    }
}
